module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        cssmin: {
            target: {
                files: [{
                    './public/stylesheets/build/output.min.css': 
                        [
                            './public/stylesheets/vendor/bootstrap/bootstrap.css',
                            './public/stylesheets/vendor/adminLte/AdminLTE.css',
                            './public/stylesheets/vendor/adminLte/skins/skin-blue.css',
                            './public/stylesheets/vendor/nprogress/nprogress.css'
                        ]
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // Default task(s).
    grunt.registerTask('default', ['cssmin']);

};