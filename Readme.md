# Express, Browserify, Browserify-shim, Backbone, Jstify templates

Express single page app with Backbone, using Browserify to create one
js file, instead of having tons of server requests pointing to each js file.

**AdminLte** pkg scripts needs to be transformed to require('adminLte')

**AdminLte depends:**
* Jquery
* Bootstrap

Browserify can expose Jquery and transform adminLte script to node style.

**Jstify** pkg to transform underscore templates to node "require" style.
**grunt-cssmin** pkg to minimize and concatenate css files

#### Packages

* [Express](http://expressjs.com/) - fast node.js network app framework
* [AdminLte](https://almsaeedstudio.com/themes/AdminLTE/index2.html) Bootstrap Admin page
* [Bootstrap](http://getbootstrap.com/) Twitter Bootstrap
* [Browserify](http://browserify.org/) Browserify node_modules for Browser
* [Browserify-shim](https://github.com/thlorenz/browserify-shim) Browserify non AMD modules like AdminLte
* [Jstify](https://github.com/zertosh/jstify) create modules of pre-compiled Underscore templates.
* [Backbone](http://backbonejs.org/) no more spaghetti
* [Grunt cssmin](https://www.npmjs.com/package/grunt-contrib-cssmin) Grunt plugin to min & concat css files

#### Install app, Start Express Server
```sh
$ git clone git@bitbucket.org:okahei/browserify-shim-backbone-adminlte.git
$ cd browserify-shim-backbone-adminlte/
$ npm install
$ npm start
```

Visit [New Running Express App](http://127.0.0.1:3000)

### Backbone

Index.js [Entry point](https://bitbucket.org/okahei/browserify-shim-backbone-adminlte/src/9845d6beba60c80a29fb2ff5259aa7ff36f918bf/public/javascripts/index.js)
initialize BackBone [Router.js](https://bitbucket.org/okahei/browserify-shim-backbone-adminlte/src/9845d6beba60c80a29fb2ff5259aa7ff36f918bf/public/javascripts/Controllers/router.js)

Routes.js [route file](https://bitbucket.org/okahei/browserify-shim-backbone-adminlte/src/9845d6beba60c80a29fb2ff5259aa7ff36f918bf/public/javascripts/routes.js)
is a tree of routes used by [breadcrumb](https://bitbucket.org/okahei/browserify-shim-backbone-adminlte/src/9845d6beba60c80a29fb2ff5259aa7ff36f918bf/public/javascripts/libs/breadCrumb.js) to populate breadcrumb model for every page change

**BackBone.sync** has been overridden to display loading spinner
[BackBone.sync-override](https://bitbucket.org/okahei/browserify-shim-backbone-adminlte/src/9845d6beba60c80a29fb2ff5259aa7ff36f918bf/public/javascripts/libs/backbone-spinner.js)
code borrowed from [here](https://github.com/cofounders/backbone-loading)
### Browserify-shim AdminLte

App.js script from AdminLte is not ready for AMD style.

Browserify can transform it to require style,
AdminLte depends on Jquery and bootstrap so in package.json we add:

```json
"browserify": {
    "transform": [
      "browserify-shim"
    ],
    "jquery": "global:$"
  },
  "browser": {
    "jquery": "./node_modules/jquery/dist/jquery.js",
    "bootstrap": "./node_modules/bootstrap/dist/js/bootstrap.js"
  },
  "browserify-shim": {
    "jquery": "$",
    "pulic/javascripts/vendor/adminLte/app.js": {
      "exports": "adminLte",
      "depends": ["jquery","bootstrap"]
    }
```

### Browserify app:
Finally we can create our bundle.js file.

```sh
$ cd browserify-shim-backbone-adminlte/
$ browserify ./public/javascripts/index.js -t jstify > public/javascripts/bundle.js
```

More simple with **"scripts"** npm task form package.json:
```sh
$ npm run browserify-build
```

### Grunt Css-min
```sh
$ npm install -g grunt-cli
$ npm install grunt --save-dev
```

##### Setup Gruntfile.js
```sh
 grunt.initConfig({
        cssmin: {
            target: {
                files: [{
                    './public/stylesheets/build/output.min.css':
                        [
                            './public/stylesheets/vendor/bootstrap/bootstrap.css',
                            './public/stylesheets/vendor/adminLte/AdminLTE.css',
                            './public/stylesheets/vendor/adminLte/skins/skin-blue.css'
                        ]
                }]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // Default task(s).
    grunt.registerTask('default', ['cssmin']);
```

##### And run it:
```sh
$ grunt -v
```

or with **"scripts"** npm task form package.json:
```sh
$ npm run css-build
```

## Jstify underscore templates into browserify

#### Compile Underscore templates
With Jstify we can compile underscore templates, allowing us to do:
```js
var myTemplate = require('templates/foo.html');
```

#### Use underscore templates Backbone view
```js
var tpl = require('../templates/msg.html');
this.$el.html(_.template(tpl(this.model.toJSON())));
```