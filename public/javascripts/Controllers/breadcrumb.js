var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var tpl = require('../templates/breadcrumb.html');


module.exports = Backbone.View.extend({
    el: '.breadcrumb',
    initialize: function () {
        window.app.activeViews.activate('breadcrumb');
        this.listenTo(this.collection, "reset", function (d) {
            this.render();
        });

        this.render();
    },
    render: function () {
        this.$el.html(_.template(tpl({models: this.collection.toJSON()})));
        return this;
    }
});
