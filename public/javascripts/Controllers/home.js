var $ = require('jquery');
var    _ = require('underscore');
var Backbone = require('backbone');
var BackboneSync = require('../libs/backbone-spinner');


module.exports = Backbone.View.extend({
    el: '.page-content',
    render: function () {
        this.$el.html(_.template("<h2>Welcome</h2>"));
        return this;
    }
});