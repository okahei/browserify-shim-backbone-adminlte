/**
 * Created by oka on 29/01/17.
 */
//var play =require('../play');
var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var tpl = require('../templates/msg.html');

module.exports = Backbone.View.extend({
    el: '#msg',
    initialize: function () {
        this.listenTo(this.model, "change", function (d) {
            if (this.model.get('msg') == '') {
                this.$el.empty();
                window.app.openAlert = false;
                return;
            }
            this.render();
        });
    },
    events: {
        "click #close-alert": function (e) {
            window.app.openAlert = false;
            console.log('On close Alert');
        }
    },
    render: function () {
        window.app.openAlert = true;
        $('.content').prepend(this.$el.html(_.template(tpl(this.model.toJSON()))));
        return this;
    }
});