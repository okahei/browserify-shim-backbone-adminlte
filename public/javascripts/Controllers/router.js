
"use strict";

var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var app = require('../app');
var routes = require('../routes');

var Router = Backbone.Router.extend({

    routes: {
        "": "home",
        "home": "home",
        "servers": "servers",
        "server/:id": "server"
    },
    /**
     * Execute will run BEFORE running any code at the route function
     *
     * @param callback route(), home(), login()...
     * @param args possible arguments
     * @param name Route name
     */
    execute: function(callback, args, name) {
        app.closeOpenAlert(); //Need to run BEFORE and not async..
        if (callback) callback.apply(this, args);
    },
    initialize: function (options) {

        /**
         * this.on('route')() code RUNS async
         * /#home --> execute async ---> this.on(route)
         *        --> execute async ---> home()
         * problem: close possible open alert on page change
         *          and setting a new alert will not show it
         *          because this.on('route')() will run mostly
         *          AFTER executing home() route code
         */
        this.on('route', function (name, params) {

            /* Set BreadCrumb*/
            var navigator = app.crumbs.getCrumbs(name,params);

            app.breadcrumb(navigator).then(function(){
                /* Set Title*/
                var size = app.models.activeBread.models.length;
                document.title = app.models.activeBread.models[size-1].attributes.title;
            });

            /* Init Frame (emails, notifications) */
            app.frame.init();
        });
    },
    home: function () {
        console.log('Router Home...');
        app.h1("Home Page");
        var HomeView = require('../Controllers/home');
        var homeView = new HomeView();
        homeView.render();
        var msg = {is_err: true, msg: 'foo'};
        app.showAlert(msg);
    },
    servers: function () {
        console.log('Router #servers');
        app.h1("Servers List");

        app.collections.Servers = require('../Models/server').Servers;
        app.collections.servers = new app.collections.Servers();
        app.views.Servers = require('../Controllers/servers');
        app.collections.servers.fetch().then(function(d){
            app.views.servers = new app.views.Servers({collection: app.collections.servers});
            app.views.servers.render();
        });
    },
    server: function (id) {
        console.log('Router #server/' + id);
        app.models.Server = require('../Models/server').Server;
        app.models.server = new app.models.Server({id: id});
        app.views.Server = require('../Controllers/server');

        app.models.server.fetch().then(function(d){
            app.views.server = new app.views.Server({model: app.models.server});
            app.views.server.render();
            app.h1(app.models.server.attributes.name);
        }, function(error){
            console.error(error);
             app.showAlert({is_err: true, msg: error.statusText});
        });
    }

});

module.exports = Router;