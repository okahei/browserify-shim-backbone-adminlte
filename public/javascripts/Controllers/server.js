var $ = require('jquery');
var    _ = require('underscore');
var Backbone = require('backbone');
var BackboneSync = require('../libs/backbone-spinner');
var tpl = require('../templates/server.html');


module.exports = Backbone.View.extend({
    el: '.page-content',
    render: function () {
        this.$el.html(_.template(tpl(this.model.toJSON())));
        return this;
    }
});
