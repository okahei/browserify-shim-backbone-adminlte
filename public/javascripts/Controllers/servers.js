var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var tpl = require('../templates/servers.html');
var BackboneSync = require('../libs/backbone-spinner');

module.exports = Backbone.View.extend({
    el: '.page-content',
    render: function () {
        this.$el.html(_.template(tpl(this.collection.toJSON())));
        return this;
    }
});
