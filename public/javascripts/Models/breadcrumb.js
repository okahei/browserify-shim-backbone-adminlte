var Backbone = require('backbone');

var BreadCrumb = Backbone.Model.extend({
    title: null,
    name: null,
    url: null,
    params: null
});

var BreadCrumbCollection = Backbone.Collection.extend({
    model: BreadCrumb
});

module.exports = {
    BreadCrumb: BreadCrumb,
    BreadCrumbCollection: BreadCrumbCollection
};
