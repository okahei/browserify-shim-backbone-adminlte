/**
 * Created by oka on 29/01/17.
 */
var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
    defaults: {
        msg: '',
        is_err: false
    }
});