var Backbone = require('backbone');

var Server = Backbone.Model.extend({
    attributeId: 'id',
    urlRoot: '/api/servers/server',
    defaults:{
        id: null,
        name: null,
        role: null,
        ip: null
    }
});

var Servers = Backbone.Collection.extend({
    url: '/api/servers',
    model: Server
});

module.exports = {
    Server: Server,
    Servers: Servers
};

