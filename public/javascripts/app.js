/**
 * Created by oka on 30/01/17.
 */

var BreadCrumbModel = require('./Models/breadcrumb');
var BreadCrumbView  = require('./Controllers/breadcrumb');
var routes = require('./routes');
var treeConfig = { childrenPropertyName: 'subRoutes' };
var Crumbs = require('./libs/breadCrumb');

var MsgView = require('./Controllers/msg');
var MsgModel = require('./Models/msg');

var app = {
    views: {},
    models: {
        activeBread: null
    },
    collections: {},
    data: {},
    openAlert: false,
    framed: false
};

app.init = function () {
    console.log('Init App...');
    return this;
};

app.h1 = function (txt) {
    document.getElementsByTagName('h1')[0].innerHTML = "";
    document.getElementsByTagName('h1')[0].innerHTML = txt;
    return this;
};

app.showAlert =  function(msg){
    if(!this.models.msg) {
        this.models.msg = new MsgModel();
    }
    
    if(!this.views.msg) {
        this.views.msg = new MsgView({model: this.models.msg});
    }

    this.models.msg.set({msg: ''}); //reset it
    this.models.msg.set(msg);
};

app.frame = {
    init: function () {
        console.log("INIT FOR FRAME");

        /*Its on screen ? */
        if (app.framed) {
            console.log("FRAME is Done");
            return;
        }

        /* MSG and Email Allwais On */

        app.framed = true;
    }
};

app.closeOpenAlert = function(){
    if(this.models.msg) {
        this.models.msg.set({is_err: false, msg: ''});
    }
    return this;
};

app.activeViews = {
    actives: [],
    activate: function (viewName) {
        if (this.isActive(viewName)) return;

        this.actives.push(viewName)
    },
    isActive: function (viewName) {
        return (this.actives.indexOf(viewName) >= 0 )
    }
};

app.crumbs = new Crumbs.createBreadCrumb(routes,treeConfig);

app.breadcrumb = function (crumbs) {
    return new Promise(function (resolve, reject) {
        if (!app.models.activeBread) {
            app.models.activeBread = new BreadCrumbModel.BreadCrumbCollection(crumbs);
        } else {
            app.models.activeBread.reset(crumbs)
        }

        if (!app.activeViews.isActive('breadcrumb')) {
            var view = new BreadCrumbView({collection: app.models.activeBread});
        }
        
        return resolve();

    });
};

module.exports = app;