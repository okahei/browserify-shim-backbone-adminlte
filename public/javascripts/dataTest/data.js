var data = {};
data.emails = [
    {
        "sender": "foo@bar.com",
        "title": "Hi Viagra!",
        "body": "Plenty of viagra..."
    },
    {
        "sender": "Pepe@bar.com",
        "title": "We have to...",
        "body": "foo bar la bla"
    },
    {
        "sender": "juan@bar.com",
        "title": "Hi Viagra!",
        "body": "Plenty of viagra..."
    }
];

data.servers = [
    {
        "id": "1",
        "name": "foo",
        "ip": "127.0.0.1",
        "role": "firewall"
    },
    {
        "id": "2",
        "name": "bar",
        "ip": "127.0.0.2",
        "role": "Email"
    },
    {
        "id": "3",
        "name": "beef",
        "ip": "127.0.0.3",
        "role": "proxy"
    }
];

module.exports = data;