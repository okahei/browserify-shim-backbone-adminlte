window.app = require('./app');

var $ = require('jquery');
var bootstrap = require('bootstrap');
var adminLTE = require('./vendor/adminLte/app');
var Backbone =  require('backbone');
var Router = require('./Controllers/router');

$(document).ready(function () {
    /*
        Because hash-based history in Internet Explorer relies on an <iframe>,
        be sure to call start() only after the DOM is ready.
     */
    var router = new Router();
    Backbone.history.start();
    console.log('Dom Ready...');
});

 