/**
 * override backbone Sync Method
 * to add spinner
 * Code form:
 * https://github.com/cofounders/backbone-loading
 */
var Backbone = require('backbone');
var nprogress = require('nprogress');
var className = 'backbone-loading';

var onLoading = function () {
    nprogress.start();
    document.documentElement.classList.add(className);
};

var onComplete = function () {
    nprogress.done();
    document.documentElement.classList.remove(className);
};

var pendingAjaxCounter = 0;
var backboneSync = Backbone.sync;

Backbone.sync = function (method, model, options) {
    model.once('request', function (model, xhr, options) {
        xhr.always(function () {
            pendingAjaxCounter -= 1;
            if (pendingAjaxCounter === 0) {
                console.log('LOADIING COMMPLETE');
                onComplete();
            }
        });
    });
    pendingAjaxCounter += 1;
    if (pendingAjaxCounter === 1) {
        onLoading();
        console.log('LOADIING');
    }
    return backboneSync.apply(this, arguments);
};

module.exports = backboneSync;