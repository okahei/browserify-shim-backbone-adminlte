/**
 * Created by oka on 1/02/17.
 */

var TreeModel = require('tree-model');

function BreadCrumb(routes, config) {

    var that = this;
    this.params = []; //url params
    this.routeNode = null;
    this.config = config || {};
    this.crumbs = [];
    this.tree = new TreeModel(config);
    this.root = this.tree.parse(routes);

    /**
     * Set url string
     *
     * @param node
     * @returns {*}
     */
    this.urlCrumb = function(node){
        var url = '';

        /**
         * has url params? /foo/bar/3/baz/4
         */
        if(node.model.param){
            url += '/#' + node.model.name + '/' + that.params.shift();
        } else {
            url += '/#' + node.model.name;
        }
        return url;
    };


    /**
     * Find first Node match title === route
     * @param route
     */
    this.findNode = function(route){
        that.routeNode = that.root.first(function (node) {
            return node.model.name === route
        });
    };

    /**
     * getCrumbs
     *
     * @param route
     * @param params
     * @returns {Array}
     */
    this.getCrumbs = function(route, params) {

        that.crumbs = [];

        if(params) that.params = params;

        that.findNode(route);
        if (that.routeNode) {
            that.routeNode.getPath().forEach(function (node) {
                that.crumbs.push({
                    title: node.model.title,
                    name: node.model.name,
                    param: node.model.param,
                    url: that.urlCrumb(node) //set url
                });
            });
            return that.crumbs;
        }
    }
}
module.exports = {
    createBreadCrumb: BreadCrumb
};