/**
 * Created by oka on 1/02/17.
 */

module.exports = {
    name: 'home',
    title: 'Home Page',
    subRoutes: [
        {
            name: 'servers',
            title: 'Servers Page',
            subRoutes: [
                {
                    name: 'server',
                    title: 'Server Page',
                    param: true
                }
            ]
        },
        {
            name: 'clusters',
            title: 'Clusters Page',
            subRoutes: [
                {
                    name: 'firewalls',
                    title: 'Firewalls Page'
                }
            ]
        },
        {
            name: 'users',
            title: 'Users Page'
        },
        {
            name: 'admin',
            title: 'Admin Page',
            subRoutes: [
                {
                    name: 'roles',
                    title: 'Roles Page'
                }
            ]
        }
    ]
};