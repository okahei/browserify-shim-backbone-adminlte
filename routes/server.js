var express = require('express');
var router = express.Router();
var data = require('../public/javascripts/dataTest/data');
/* GET users listing. */
router.get('/:id', function(req, res, next) {

    /**
     * Find Server by ID
     * @type {Array.<*>}
     */
    var server = data.servers.filter(function(o){
        if(o.id == req.params.id) return o;
    });

    /**
     * Simulate network delay
     */
    setTimeout(response, 3000);

    /**
     * Response json or 404
     *
     * @returns {*}
     */
    function response(){
        if(server[0]) return res.json(server[0]);
        return next(); //404
    }
    
});

module.exports = router;
