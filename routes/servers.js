var express = require('express');
var router = express.Router();
var data = require('../public/javascripts/dataTest/data');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.json(data.servers);
});

module.exports = router;
